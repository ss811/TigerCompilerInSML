# Tiger Compiler
## ECE553 Course Project

The compiler is developed according to the instruction from the Andrew W. Appel's "Modern Compiler Implementation in ML". For more information, check http://www.cs.princeton.edu/~appel/modern/ml/

### 25/05/2018 Compiler Complete
Usage and Test: 
```
1. enter final folder
2. In sml, useing the command: CM.make "source.sm"
3. Then using Main.main <SourceProgramName> to read the source program
4. It will generate a MIPS assembly code file called <SourceProgramName>.s 
```
Current Porblem:
```
When trying compile multiple declarations of types, some of the type could not be correctly recorded, which lead to error in later translation.
```
Test: 
```
use test.tig and test1.tig in final for simple test.
```